//
//  SeeLogViewController.h
//  Haze Watch
//
//  Created by Oscar Wang on 13/10/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeeLogViewController : UIViewController{
    IBOutlet UITextField *logDate;
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIToolbar *toolBar;
    NSDate *theDate;
    IBOutlet UITextView *logView;
}

@end
