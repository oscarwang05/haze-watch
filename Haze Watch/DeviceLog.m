//
//  DeviceLog.m
//  Haze Watch
//
//  Created by Oscar Wang on 12/10/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import "DeviceLog.h"

@implementation DeviceLog
@synthesize logString;

- (NSString *)loadLogFileofDate:(NSDate *)date{
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"LogFile%@.txt",dateString]];
    [fileManager changeCurrentDirectoryPath:[documentsDirectory stringByExpandingTildeInPath]];
    if(![fileManager fileExistsAtPath:path]){
        return nil;
    }
    else{
        logString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    }
    return logString;
}

- (void)removeSelectedLogFile:(NSDate *)date{
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"LogFile%@.txt",dateString]];
    [fileManager changeCurrentDirectoryPath:[documentsDirectory stringByExpandingTildeInPath]];
    if([fileManager fileExistsAtPath:path]){
        [fileManager removeItemAtPath:path error:nil];
    }
}

- (void)saveLogFile:(NSString *)log{
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    NSDate *date = [[NSDate alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"LogFile%@.txt",dateString]];
    [fileManager changeCurrentDirectoryPath:[documentsDirectory stringByExpandingTildeInPath]];
    if(![fileManager fileExistsAtPath:path]){
        [fileManager createFileAtPath:[NSString stringWithFormat:@"LogFile%@.txt",dateString] contents:nil attributes:nil];
        [logString writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    else{
        logString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    }
    logString = [logString stringByAppendingFormat:@"%@",log];
    [logString writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

@end
