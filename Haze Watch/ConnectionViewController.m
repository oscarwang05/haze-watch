//
//  ViewController.m
//  Haze Watch
//
//  Created by Oscar Wang on 10/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import "ConnectionViewController.h"
#import "RawDataViewController.h"
#import "KeychainWrapper.h"

@interface ConnectionViewController ()

@end

@implementation ConnectionViewController
@synthesize currentNodeDeviceList;

- (void)rememberPreviousDevice:(CBPeripheral *)device
{
    NSString *UUID = device.identifier.UUIDString;
    [KeychainWrapper createKeychainValue:UUID forIdentifier:KEYCHAIN_UUID];
}

- (NSString *)connectToPreviousDevice
{
    return DEVICE_UUID;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nodeDeviceListUpdated:) name:kNodeDeviceListUpdate object:[VTNodeManager getInstance]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedThatNodeDeviceIsReady:) name:VTNodeDeviceIsReadyWithOxaNotification object:[VTNodeManager getInstance]];
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refresh addTarget:self
                action:@selector(refreshView:)
      forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refresh;
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated {
    //Grab the Node delegate
    [VTNodeManager getInstance].selectedNodeDevice.delegate = self;
    if([[VTNodeManager getInstance].selectedNodeDevice.peripheral isConnected]) {
        [VTNodeManager disconnectFromDevice:[VTNodeManager getInstance].selectedNodeDevice.peripheral];
        [VTNodeManager getInstance].selectedNodeDevice = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) nodeDeviceListUpdated:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.currentNodeDeviceList = [VTNodeManager allNodeDevices];
    return [self.currentNodeDeviceList count];
}

- (VTNodeDeviceCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"nodeDeviceCell";
    VTNodeDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[VTNodeDeviceCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
    }
    cell.cbperipheral = [self.currentNodeDeviceList objectAtIndex:indexPath.row];
    NSString *UUID = [self connectToPreviousDevice];
    if([cell.cbperipheral.identifier.UUIDString isEqualToString:UUID]){
        selectedDevice = cell.cbperipheral;
        [VTNodeManager connectToDevice:selectedDevice];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VTNodeDeviceCell *cell = (VTNodeDeviceCell *)[tableView cellForRowAtIndexPath:indexPath];
    selectedDevice = cell.cbperipheral;
    [VTNodeManager connectToDevice:selectedDevice];
    
}

-(void)refreshView:(UIRefreshControl *)refresh {
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing device list..."];
    [VTNodeManager stopFindingDevices];
    [VTNodeManager startFindingDevices];
    [self.tableView reloadData];
    [refresh endRefreshing];
}

- (void) notifiedThatNodeDeviceIsReady:(NSNotification *)notification
{
    NSLog(@"%@ - notifiedThatNodeDeviceIsReady", [self class]);
    [self rememberPreviousDevice:selectedDevice];
    RawDataViewController *rawData = [self.storyboard instantiateViewControllerWithIdentifier:@"rawData"];
    rawData.cbperipheral = selectedDevice;
    [self.navigationController pushViewController:rawData animated:YES];
}

#pragma mark - NodeDeviceDelegate

- (void) nodeDeviceIsReadyForCommunication:(VTNodeDevice *)device {
    //This is handled by notification from the Connection Manager
}

-(void)nodeDeviceFailedToInit:(VTNodeDevice *)device {
    //TODO: Handle error for failed device initialization
}

@end
