//
//  DataRecord.h
//  Haze Watch
//
//  Created by Oscar Wang on 15/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "AFHTTPRequestOperation.h"
#import "AFHTTPClient.h"

@interface DataRecord : NSObject{
    float rawData;
    NSUInteger num;
    CLLocationCoordinate2D coordinate;
}

- (void)setRawData:(float)reading;
- (void)setCoordinate:(CLLocationCoordinate2D)theLocation;
- (void)exportDatatoFile:(NSString *)filename;
- (void)postData:(NSMutableArray *)xmlStringSamples fromDeviceID:(NSString *)deviceID;
- (void)setLocationNo:(NSUInteger)no;

@end
