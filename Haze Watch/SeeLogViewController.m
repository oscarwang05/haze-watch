//
//  SeeLogViewController.m
//  Haze Watch
//
//  Created by Oscar Wang on 13/10/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import "SeeLogViewController.h"
#import "DeviceLog.h"

@interface SeeLogViewController ()

@end

@implementation SeeLogViewController

- (void)fileManagement
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
}

- (IBAction)seeLog:(id)sender{
    DeviceLog *deviceLog = [[DeviceLog alloc]init];
    NSString *logString = [deviceLog loadLogFileofDate:theDate];
    logView.text = logString;
}

- (IBAction)deleteLog:(id)sender{
    DeviceLog *deviceLog = [[DeviceLog alloc]init];
    [deviceLog removeSelectedLogFile:theDate];
}

- (IBAction)dateChanged:(id)sender {
    UIDatePicker *picker = (UIDatePicker *)sender;
    logDate.text = [NSString stringWithFormat:@"%@", picker.date];
}

- (IBAction)doneEditing:(id)sender {
    NSDateFormatter *dateTimeFormatter = [NSDateFormatter new];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *datetimeString = [NSString stringWithFormat:@"%@",[dateTimeFormatter stringFromDate:datePicker.date]];
    theDate = datePicker.date;
    logDate.text= [NSString stringWithFormat:@"%@",datetimeString];
    [logDate resignFirstResponder];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    datePicker.datePickerMode = UIDatePickerModeDate;
    logDate.inputView = datePicker;
    logDate.inputAccessoryView = toolBar;
    theDate = [[NSDate alloc]init];
    NSDateFormatter *dateTimeFormatter = [NSDateFormatter new];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *datetimeString = [NSString stringWithFormat:@"%@",[dateTimeFormatter stringFromDate:theDate]];
    logDate.text = [NSString stringWithFormat:@"%@",datetimeString];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
