//
//  DataRecord.m
//  Haze Watch
//
//  Created by Oscar Wang on 15/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import "DataRecord.h"
#import "Constants.h"
#import "XMLDictionary.h"

@implementation DataRecord

- (void)setRawData:(float)reading{
    rawData = reading;
}

- (void)setCoordinate:(CLLocationCoordinate2D)theLocation{
    coordinate = theLocation;
}

- (void)setLocationNo:(NSUInteger)no
{
    num = no;
}

- (void)exportDatatoFile:(NSString *)filename{
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    NSDateFormatter *timeFormatter = [NSDateFormatter new];
    NSDate *today = [[NSDate alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:today]];
    NSString *timeString = [NSString stringWithFormat:@"%@",[timeFormatter stringFromDate:today]];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[filename stringByAppendingFormat:@"%@.csv",dateString]];
    NSString *myData = [NSString string];
    static NSString *title = @"Location,Latitude,Longitude,RawData,Date,Time\r";
    myData = [myData stringByAppendingFormat:@"%@",title];
    [fileManager changeCurrentDirectoryPath:[documentsDirectory stringByExpandingTildeInPath]];
    if(![fileManager fileExistsAtPath:path]){
        [fileManager createFileAtPath:[NSString stringWithFormat:@"%@%@.csv",filename,dateString] contents:nil attributes:nil];
        [myData writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    else{
        myData = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    }
    NSString *coordinateString = [NSString stringWithFormat:@"%d,%f,%f,%f,%@,%@\r",num,coordinate.latitude,coordinate.longitude,rawData,dateString,timeString];
    myData = [myData stringByAppendingFormat:@"%@",coordinateString];
    [myData writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (void)postData:(NSMutableArray *)xmlStringSamples fromDeviceID:(NSString *)deviceID
{
    NSString *xmlString = [NSString string];
    /*
    NSMutableDictionary *dataDic = [NSMutableDictionary dictionary];
    NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
    NSMutableDictionary *sampleDic = [NSMutableDictionary dictionary];
    [infoDic setObject:@"debug" forKey:@"user_name"];
    [infoDic setObject:@"" forKey:@"comment"];
    [infoDic setObject:deviceID forKey:@"group"];
    
    [sampleDic setObject:xmlStringSamples forKey:@"samples"];
    
    [dataDic setObject:infoDic forKey:@"info"];
    [dataDic setObject:xmlStringSamples forKey:@"samples"];
    NSDictionary *contentDic = [NSDictionary dictionaryWithObject:dataDic forKey:@"data"];
    
    xmlString = [contentDic XMLString];
    */
    NSString *xmlStringHeader = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?><data><info><user_name>debug</user_name><comment></comment><group>%@</group></info><samples>",deviceID];
    NSString *xmlStringFooter = @"</samples></data>";
    
    xmlString = [xmlString stringByAppendingString:xmlStringHeader];
    for (NSString *sample in xmlStringSamples) {
        xmlString = [xmlString stringByAppendingString:sample];
    }
    xmlString = [xmlString stringByAppendingString:xmlStringFooter];
    NSDictionary *dic = [NSDictionary dictionaryWithObject:xmlString forKey:@"content"];
    
    AFHTTPClient *client = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:serverURLString]];
    [client postPath:@"/post-data.php" parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *responseData = [operation responseData];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
