//
//  ViewController.h
//  Haze Watch
//
//  Created by Oscar Wang on 10/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"
#import "VTNodeDeviceCell.h"

@interface ConnectionViewController : UITableViewController<NodeDeviceDelegate>{
    CBPeripheral *selectedDevice;
}
@property (strong, nonatomic) NSArray *currentNodeDeviceList;

@end
