//
//  AppDelegate.h
//  Haze Watch
//
//  Created by Oscar Wang on 10/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ApplicationDelegate

@optional

- (void)applicationDidEnterBackground:(UIApplication *)application;
- (void)applicationWillEnterForeground:(UIApplication *)application;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) id<ApplicationDelegate> delegate;

@end

