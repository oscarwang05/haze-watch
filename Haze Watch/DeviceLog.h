//
//  DeviceLog.h
//  Haze Watch
//
//  Created by Oscar Wang on 12/10/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceLog : NSObject

@property(nonatomic,readonly)NSString *logString;

- (NSString *)loadLogFileofDate:(NSDate *)date;
- (void)removeSelectedLogFile:(NSDate *)date;
- (void)saveLogFile:(NSString *)log;

@end
