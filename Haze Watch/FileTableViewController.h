//
//  FileTableViewController.h
//  Haze Watch
//
//  Created by Oscar Wang on 6/12/2013.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface FileTableViewController : UITableViewController<MFMailComposeViewControllerDelegate>{
    NSArray *_fileArray;
}

@property(nonatomic,retain)NSArray *fileArray;

@end
