//
//  RawDataViewController.m
//  Haze Watch
//
//  Created by Oscar Wang on 14/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import "RawDataViewController.h"
#import "DeviceLog.h"
#import "Constants.h"
#import "SeeLogViewController.h"
#import "AppDelegate.h"

#define GET_DATA_TIME 5000              //How frequently should iPhone fetch data from Node device, unit in ms
#define LINEAR_ALGORITHM 1
#define ACCELERATION_ALGORITHM 2
#define SIGNAL_STRENGTH_LOWER_THRESHOLD 3
#define SIGNAL_STRENGTH_UPPER_THRESHOLD 100
#define BASELINE 0.200644f

#define CO 0
#define NO2 1
#define PM10 2
#define PM2_5 3

@interface RawDataViewController ()<ApplicationDelegate>{
    NSString *deviceID;
}

@end

@implementation RawDataViewController
@synthesize cbperipheral;
@synthesize startRecording;

- (void)navigationBarSetup
{
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"See Log" style:UIBarButtonItemStylePlain target:self action:@selector(gotoLogFile:)];
    rightBarButtonItem.enabled = NO;
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (IBAction)handleTimer:(id)sender
{
    if(isRecording){
        NSTimeInterval timeInterval = [now timeIntervalSinceNow];
        _timerLabel.text = [self calculateTimeInterval:timeInterval];
    }
}

- (IBAction)gotoLogFile:(id)sender
{
    SeeLogViewController *seeLog = [[SeeLogViewController alloc]init];
    [self.navigationController pushViewController:seeLog animated:YES];
}

- (IBAction)mapViewFullScreen:(id)sender
{
    [UIView animateWithDuration:0.5f animations:^{
        _mapView.frame = self.view.frame;
    }];
    [self.view bringSubviewToFront:_mapView];
    self.navigationController.navigationBar.hidden = YES;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.tintColor = [UIColor blueColor];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    //button.titleLabel.font = [UIFont systemFontOfSize:14];
    button.frame = CGRectMake(20, 30, 60, 30);
    button.layer.cornerRadius = 10.0f;
    button.layer.borderColor = [[UIColor blueColor]CGColor];
    button.layer.borderWidth = 1.0f;
    [button addTarget:self action:@selector(mapViewDeFullScreen:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (IBAction)mapViewDeFullScreen:(id)sender
{
    UIButton *button = sender;
    [button removeFromSuperview];
    
    [UIView animateWithDuration:0.5f animations:^{
        _mapView.frame = CGRectMake(0, 54, 320, 177);
    }completion:^(BOOL finished){
        [self.view sendSubviewToBack:_mapView];
        self.navigationController.navigationBar.hidden = NO;
    }];

}

- (void)getNodeInfo:(NSString *)nodeName
{
    //NSString *device_id = [NSString stringWithFormat:@"%d",[[nodeName substringFromIndex:5]integerValue] + 100];
    //NSURL *serverUrl = [NSURL URLWithString:@"http://www.pollution.ee.unsw.edu.au/get-calib-constants.php"];
    AFHTTPClient *client = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:serverURLString]];
    
    [client postPath:@"/get-calib-constants.php" parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSData *responseData = [operation responseData];
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc]init];
        NSDictionary *responseDic = [parser dictionaryWithData:responseData];
        NSArray *responseArray = [responseDic objectForKey:@"device_values"];
        int count;
        count = 0;
        for(NSDictionary *dic in responseArray){
            id coDic = [dic objectForKey:@"co"];
            NSString *deviceName;
            if(coDic){
                if([coDic isKindOfClass:[NSArray class]]){
                    deviceName = [[coDic objectAtIndex:0]objectForKey:@"device_name"];
                }
                else if([coDic isKindOfClass:[NSDictionary class]]){
                    deviceName = [coDic objectForKey:@"device_name"];
                }
            }
            else{
                deviceName = @"";
            }
            if([deviceName isEqualToString:deviceID]){
                offset = [[coDic objectForKey:@"offset"]floatValue];
                DLog(@"Obtained offset = %f\n",offset);
                break;
            }
            count++;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"%@",error);
    }];
}

- (void)saveDatatoLog:(NSString *)logString
{
    DeviceLog *deviceLog = [[DeviceLog alloc]init];
    [deviceLog saveLogFile:logString];
}

- (NSString *)calculateTimeInterval:(NSTimeInterval)timeInterval
{
    NSString *timeString;
    int duration = abs((int)timeInterval);
    //  minutes:    60
    //  hours:      3600
    //  days:       86400
    //  months:     2592000
    //  years:      946080000
    
    int hour,minute,second;
    hour = duration/3600;
    minute = (duration - hour * 3600)/60;
    second = duration - hour * 3600 - minute * 60;
    
    timeString = [NSString stringWithFormat:@"%02d:%02d:%02d",hour,minute,second];
    
    return timeString;
}

- (void)saveToFile:(NSString *)content
{
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSString *logString;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    NSDate *today = [[NSDate alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:today]];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Location Log%@.txt",dateString]];
    if(![fileManager fileExistsAtPath:path]){
        [fileManager createFileAtPath:[NSString stringWithFormat:@"Location Log%@.txt",dateString] contents:nil attributes:nil];
    }
    else{
        logString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    }
    logString = [logString stringByAppendingString:content];
    [logString writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

#pragma mark-
#pragma MKMapView Delegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    /*
    theRegion.center = userLocation.location.coordinate;
    theRegion.span = MKCoordinateSpanMake(0.02, 0.02);
    //[mapView setRegion:theRegion animated:YES];
    latitude.text = [NSString stringWithFormat:@"%f",userLocation.location.coordinate.latitude];
    longitude.text = [NSString stringWithFormat:@"%f",userLocation.location.coordinate.longitude];
    DLog(@"\nUpdated Location:%@,%@",latitude.text,longitude.text);
     */
}

#pragma CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    if (fabs([newLocation.timestamp timeIntervalSinceNow]) > 30.0f) {
        // it is invalid location, ignore
        NSString *string = [NSString stringWithFormat:@"Location Lost at %@",[NSDate date]];
        [self saveDatatoLog:string];
        return;
    }
    theRegion.center = newLocation.coordinate;
    latitude.text = [NSString stringWithFormat:@"%f",theRegion.center.latitude];
    longitude.text = [NSString stringWithFormat:@"%f",theRegion.center.longitude];
    DLog(@"\nUpdated Location:%@,%@",latitude.text,longitude.text);
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    DLog(@"Location update paused");
}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    DLog(@"Location update resumed");
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error domain] == kCLErrorDomain)
    {
        switch ([error code])
        {
            case kCLErrorDenied:
                [_locationManager stopUpdatingLocation];
                break;
            case kCLErrorLocationUnknown:
                break;
            default:
                break;
        }
    }
}

- (void)refreshData:(float)reading
{
    ppmData.text = [NSString stringWithFormat:@"%f",reading];
}

- (IBAction)toggleRecording
{
    if(isRecording){
        isRecording = NO;
        [startRecording setImage:[UIImage imageNamed:@"red_button"] forState:UIControlStateNormal];
        [startRecording setTitle:@"Start Recording" forState:UIControlStateNormal];
    }
    else{
        isRecording = YES;
        _timerLabel.text = @"00:00:00";
        now = [[NSDate alloc]init];
        [startRecording setImage:[UIImage imageNamed:@"green_button"] forState:UIControlStateNormal];
        startRecording.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [startRecording setTitle:@"Stop Recording" forState:UIControlStateNormal];
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(handleTimer:) userInfo:self repeats:YES];
    }
}

- (NSString *)synthesisSampleDatawithLatitude:(float)lat
                             Longitude:(float)lng
                                  Time:(NSString *)datetimeString
                              PPMValue:(float)ppmValue
{
    NSString *xmlStringSample = [NSString stringWithFormat:@"<sample><datetime>%@</datetime><latitude>%f</latitude><longitude>%f</longitude><%@>%f</%@></sample>",datetimeString,lat,lng,pollutantType,ppmValue,pollutantType];
    return xmlStringSample;
}

- (void)tunnelDataUploading
{
    int i;
    CLLocationCoordinate2D theLastLocation;
    NSString *xmlString;
    NSUInteger dataNo = [tunnelData count];
    float unitLatitude = (stopLocation.latitude - startLocation.latitude)/dataNo;
    float unitLongitude = (stopLocation.longitude - startLocation.longitude)/dataNo;
    theLastLocation = startLocation;
    for(i=0;i<dataNo;i++){
        theLastLocation.latitude = theLastLocation.latitude + unitLatitude;
        theLastLocation.longitude = theLastLocation.longitude + unitLongitude;
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
        annotation.coordinate = theLastLocation;
        [_mapView addAnnotation:annotation];
        [tunnelLatitude addObject:[NSString stringWithFormat:@"%f",theLastLocation.latitude]];
        [tunnelLongitude addObject:[NSString stringWithFormat:@"%f",theLastLocation.longitude]];
    }
    
    for(i=0;i<dataNo;i++){
        xmlString = [self synthesisSampleDatawithLatitude:[[tunnelLatitude objectAtIndex:i]floatValue]
                                               Longitude:[[tunnelLongitude objectAtIndex:i]floatValue]
                                                    Time:[tunnelDatetime objectAtIndex:i]
                                                PPMValue:[[tunnelData objectAtIndex:i]floatValue]];
        [xmlSamples addObject:xmlString];
        NSLog(@"%@\n",xmlString);
    }
    [myData postData:xmlSamples fromDeviceID:deviceID];
    [xmlSamples removeAllObjects];
}

#pragma NODE delegate method

-(void)nodeDeviceDidUpdateGyroReading:(VTNodeDevice *)device withReading:(VTSensorReading *)reading{
    x = reading.x;
    y = reading.y;
    accX.text = [NSString stringWithFormat:@"%.3f",x];
    accY.text = [NSString stringWithFormat:@"%.3f",y];
    NSLog(@"%.3f %.3f",x,y);
}

-(void)nodeDeviceDidUpdateOxaReading:(VTNodeDevice *)device withReading:(float)reading
{
    float co_response_ratio = 39.0f;
    float co_tia_gain = 35000.0f;
    float co_base_line = offset;
    
    float ppm = ((reading - co_base_line) / 0.37736 / (co_tia_gain *
                                                       co_response_ratio)) * 1E9;
    if(ppm < 0){
        ppm = 0;
    }
    //DLog(@"PPM = %f,Raw = %f\n",ppm,reading);
    rawData.text = [NSString stringWithFormat:@"%.3f",reading];
    [self refreshData:ppm];
    lastLocation = theRegion.center;
    
    //theRegion.center = _mapView.userLocation.coordinate;
    
    if((theRegion.center.latitude + theRegion.center.longitude) <= 10){
        theRegion.center = lastLocation;
    }
    latitude.text = [NSString stringWithFormat:@"%f",theRegion.center.latitude];
    longitude.text = [NSString stringWithFormat:@"%f",theRegion.center.longitude];
    //[_mapView setRegion:theRegion];
    NSDate *today = [[NSDate alloc]init];
    NSDateFormatter *dateTimeFormatter = [NSDateFormatter new];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *datetimeString = [NSString stringWithFormat:@"%@",[dateTimeFormatter stringFromDate:today]];
    
    NSString *xmlString = @"";
    if(isRecording){
        locationNo++;
        [myData setLocationNo:locationNo];
        [myData setRawData:ppm];
        [myData setCoordinate:theRegion.center];
        //CLLocationCoordinate2D coordinate = theRegion.center;

        if(tunnelStart == NO){
            [myData exportDatatoFile:@"Coordinate"];
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
            annotation.coordinate = theRegion.center;
            [_mapView addAnnotation:annotation];
            xmlString = [self synthesisSampleDatawithLatitude:theRegion.center.latitude
                                                   Longitude:theRegion.center.longitude
                                                        Time:datetimeString
                                                    PPMValue:ppm];
            [xmlSamples addObject:xmlString];
            NSLog(@"%@\n",xmlString);
            if([xmlSamples count] >=  5)
            {
                [myData postData:xmlSamples fromDeviceID:deviceID];
                NSLog(@"I just sent %d samples to the server\n",[xmlSamples count]);
                [xmlSamples removeAllObjects];
            }
        }
    
        // Tunnel recognazation
        // This is the line algorithm, despite I don't like it, I still put it here in case, later I will put a setting button to switch between this algorithm with acceleration one, that one is much more common and useful, I really don't understand why we need this thing....
        if(mode == LINEAR_ALGORITHM){
            //gpsStrength <= SIGNAL_STRENGTH_LOWER_THRESHOLD || 
            if(gpsStrength >= SIGNAL_STRENGTH_UPPER_THRESHOLD){
            //if((lastLocation.latitude == theRegion.center.latitude && lastLocation.longitude == theRegion.center.longitude) ||((theRegion.center.latitude + theRegion.center.longitude) < 5)){
                if(tunnelStart == NO){
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    NSDate *date = [[NSDate alloc]init];
                    [dateFormatter setDateFormat:@"HH:mm:ss"];
                    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
                    NSString *logString = [NSString stringWithFormat:@"[%@]Tunnel Start with\nLatitude = %f\nLogitude = %f\nExposure Value = %f\n\n",dateString,startLocation.latitude,startLocation.longitude,ppm];
                    [self saveDatatoLog:logString];
                    startLocation = theRegion.center;
                    tunnelStart = YES;
                }
                thisLocationNo = locationNo;
                NSString *tunnelString = [NSString stringWithFormat:@"%f",ppm];
                [tunnelData addObject:tunnelString];
                [tunnelDatetime addObject:datetimeString];

                tunnelSwitch.text = @"ON";
                tunnelSwitch.textColor = [UIColor redColor];
            }
            else{
                if(tunnelStart == YES){
                    tunnelStart = NO;
                    tunnelStop = YES;
                    tunnelSwitch.textColor = [UIColor redColor];
                    tunnelSwitch.text = @"OFF";
                    stopLocation = theRegion.center;
                    NSString *tunnelString = [NSString stringWithFormat:@"%f",ppm];
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    NSDate *date = [[NSDate alloc]init];
                    [dateFormatter setDateFormat:@"HH:mm:ss"];
                    NSString *dateString = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
                    NSString *logString = [NSString stringWithFormat:@"[%@]Tunnel Stop with\nLatitude = %f\nLogitude = %f\nExposure Value = %@\n\n",dateString,startLocation.latitude,startLocation.longitude,tunnelString];
                    [self saveDatatoLog:logString];

                    [self tunnelDataUploading];
                }
            }
            if(tunnelStop){
                tunnelStop = NO;
            }
            
        }
        
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{   
    [super viewDidLoad];
    [self navigationBarSetup];
    xmlSamples = [[NSMutableArray alloc]init];
    [[VTNodeManager getInstance].selectedNodeDevice setDelegate:self];
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    theRegion.span = MKCoordinateSpanMake(0.02, 0.02);
    theRegion.center = _mapView.userLocation.coordinate;
    [_mapView setRegion:theRegion animated:YES];
    startRecording.imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = _mapView.frame;
    button.backgroundColor = [UIColor clearColor];
    [button addTarget:self action:@selector(mapViewFullScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    [self.view bringSubviewToFront:button];
    mode = LINEAR_ALGORITHM;
    isRecording = NO;
    myData = [[DataRecord alloc]init];
    locationNo = 0;
    [[VTNodeManager getInstance].selectedNodeDevice setStreamModeOxa:YES
                                                       forModulePort:VTModuleLocationB
                                                         withTiaGain:5
                                                           withRLoad:0
                                                       withRefSource:1
                                                            withIntZ:00
                                                        withBiasSign:1
                                                            withBias:0
                                                        withFetShort:0
                                                          withOpMode:3
                                                          withPeriod:GET_DATA_TIME/10
                                                        withLifetime:0];
    tunnelSwitch.textColor = [UIColor redColor];
    tunnelData = [[NSMutableArray alloc]init];
    tunnelLatitude = [[NSMutableArray alloc]init];
    tunnelLongitude = [[NSMutableArray alloc]init];
    tunnelDatetime = [[NSMutableArray alloc]init];
    pollutantType = @"co";
    [self getNodeInfo:[VTNodeManager getInstance].selectedNodeDevice.peripheral.name];
    deviceID = [VTNodeManager getInstance].selectedNodeDevice.peripheral.name;
    offset = 0;
    startRecording.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.title = [VTNodeManager getInstance].selectedNodeDevice.peripheral.name;
    _timerLabel.text = @"00:00:00";
    
    
    _locationManager = [[CLLocationManager alloc]init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 1.0f;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    _locationManager.pausesLocationUpdatesAutomatically = NO;
    [_locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
