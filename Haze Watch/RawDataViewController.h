//
//  RawDataViewController.h
//  Haze Watch
//
//  Created by Oscar Wang on 14/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Node iOS/Node.h>
#import <MapKit/MapKit.h>
#import "DataRecord.h"
#import "VTNodeManager.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "XMLDictionary.h"

@interface RawDataViewController : UIViewController<NodeDeviceDelegate,UIAccelerometerDelegate,NSXMLParserDelegate,CLLocationManagerDelegate,MKMapViewDelegate,CLLocationManagerDelegate>{
    VTNodeDevice *myDevice;
    IBOutlet MKMapView *_mapView;
    BOOL isRecording;
    DataRecord *myData;
    NSUInteger locationNo;
    CLLocationCoordinate2D startLocation,stopLocation,lastLocation;
    NSUInteger thisLocationNo,mode;
    BOOL tunnelStart,tunnelStop;
    float x,y,gpsStrength;
    IBOutlet UILabel *latitude;
    IBOutlet UILabel *longitude;
    IBOutlet UILabel *accX;
    IBOutlet UILabel *accY;
    IBOutlet UILabel *gpsSignalStrenth;
    IBOutlet UILabel *rawData;
    IBOutlet UILabel *ppmData;
    NSMutableArray *xmlSamples;
    NSMutableArray *tunnelData,*tunnelLatitude,*tunnelLongitude,*tunnelDatetime;
    IBOutlet UILabel *tunnelSwitch;
    MKCoordinateRegion theRegion;
    IBOutlet UISegmentedControl *pollutant;
    NSString *pollutantType;
    CGFloat offset;
    CLLocationManager *_locationManager;
    NSTimer *timer;
    IBOutlet UILabel *_timerLabel;
    NSDate *now;
}

@property(strong,nonatomic)CBPeripheral *cbperipheral;
@property(nonatomic,retain)IBOutlet UIButton *startRecording;
@property(nonatomic,retain)CLLocationManager *locationManager;

@end

