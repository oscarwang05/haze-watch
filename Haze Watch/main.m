//
//  main.m
//  Haze Watch
//
//  Created by Oscar Wang on 10/07/13.
//  Copyright (c) 2013 UNSW. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
