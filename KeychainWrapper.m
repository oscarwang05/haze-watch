//
//  KeychainWrapper.m
//
//  Created by Jeffrey Leung on 20/09/13.
//

#import "KeychainWrapper.h"

@implementation KeychainWrapper

+ (NSMutableDictionary *)setupSearchDirectoryForIdentifier:(NSString *)identifier
{
	// SEtup dictionary to access keychain
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    // Specify we are using a password (rather than a certificate, internet password, etc.)
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    // Uniquely identify this keychain accessor
    [searchDictionary setObject:APP_NAME forKey:(__bridge id)kSecAttrService];
    
    // Uniquely identify the account who wil be accessing the keychain
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    
    return searchDictionary;
    
}

+ (NSData *)searchKeychainCopyMatchingIdentifier:(NSString *)identifier
{
    NSMutableDictionary *searchDictionary = [self setupSearchDirectoryForIdentifier:identifier];
    
    // Limit search results to one.
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    
    // Specify we want NSdata/CFData returned
    [searchDictionary setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    // Search
    NSData *result = nil;
    CFTypeRef foundDict = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary, &foundDict);
    
    if (status == noErr)
    {
        result = (__bridge_transfer NSData *)foundDict;
    }
    else
    {
        result = nil;
    }
    
    return result;
}


+ (NSString *)keychainStringFromMatchingIdentifier:(NSString *)identifier
{
    NSData *valueData = [self searchKeychainCopyMatchingIdentifier:identifier];
    if (valueData)
    {
        NSString *value = [[NSString alloc] initWithData:valueData encoding:NSUTF8StringEncoding];
        return value;
    }
    else
    {
        return nil;
    }
}

+ (BOOL)compareKeychainValueForMatchingIdentifier:(NSString *)identifier andHash:(NSUInteger)pinHash
{
	if ([[self keychainStringFromMatchingIdentifier:identifier] isEqualToString:[self securedSHA256DigestHashForPIN:pinHash]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (BOOL)createKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier
{
	NSMutableDictionary *dictionary = [self setupSearchDirectoryForIdentifier:identifier];
    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:valueData forKey:(__bridge id)kSecValueData];
    
    // protect the keychain entry so it's only valid when the device is unlocked
    [dictionary setObject:(__bridge id)kSecAttrAccessibleWhenUnlocked forKey:(__bridge id)kSecAttrAccessible];
    
    // Add
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
    
    // If the addition was cuccesful return. otherwise attemp to update existing key or quit
    if (status == errSecSuccess)
    {
        return YES;
    }
    else if (status == errSecDuplicateItem)
    {
        return [self updateKeychainValue:value forIdentifier:identifier];
    }
    else
    {
        return NO;
    }
}

+ (BOOL)updateKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier
{
	NSMutableDictionary *searchDictionary = [self setupSearchDirectoryForIdentifier:identifier];
    NSMutableDictionary *updateDictionary = [[NSMutableDictionary alloc] init];
    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [updateDictionary setObject:valueData forKey:(__bridge id)kSecValueData];
    
    // Update
    OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)searchDictionary,
                                    (__bridge CFDictionaryRef)updateDictionary);
    
    if (status == errSecSuccess)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

// Delete a value in the keychain.
+ (void)deleteItemFromKeychainWIthIdentifier:(NSString *)identifier
{
    NSMutableDictionary *searchDictionary = [self setupSearchDirectoryForIdentifier:identifier];
    CFDictionaryRef dictionary = (__bridge CFDictionaryRef)searchDictionary;
    
    // Delete
    SecItemDelete(dictionary);
}

// Generates an SHA256 (much more secure than MD5) hash.
+ (NSString *)securedSHA256DigestHashForPIN:(NSUInteger)pinHash
{

    NSString *computedHashString = [NSString stringWithFormat:@"%i%@", pinHash, SALT_HASH];

    NSString *finalHash = [self computeSHA256DigestForString:computedHashString];
    NSLog(@"** Computed hash: %@ for SHA256 Digest: %@", computedHashString, finalHash);
    return finalHash;
}

+ (NSString *)computeSHA256DigestForString:(NSString *)input
{
	const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    
    // THis is an ios5 specific method
    CC_SHA256(data.bytes, data.length, digest);
    
    // Setup Objective-C output
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    // PARSE THROUGH THE CC_SHA256 RESULTs (stored inside of digest[])
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}



@end
