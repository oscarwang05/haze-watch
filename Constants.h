//
//  Constants.h
//  Meetisan
//
//  Created by Jeffrey Leung on 16/10/13.
//  Copyright (c) 2013 Yan Wang. All rights reserved.
//

// Used to specify the application used in accessing keychain

#define APP_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]

#define SALT_HASH @"FvTivqTqZXsgLLx1v3P8TGRyVHaSOB1pvfm02wvGadj7RLHV8GrfxaZ84oGA8RsKdNRpxdAojXYg9iAj"

// Keychain identifiers
#define KEYCHAIN_UUID   @"kcUUID"

#define DEVICE_UUID [KeychainWrapper keychainStringFromMatchingIdentifier:KEYCHAIN_UUID]

// Userful Variable identifiers
#define DEFAULT_COLOR [UIColor colorWithRed:85/255.0 green:161/255.0 blue:204/255.0 alpha:1]
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define IPHONE5_SIZE kScreenHeight == 568
#define IPHONE4_SIZE kScreenHeight == 480
#define FIRST_ROW_AND_SECTION [NSIndexPath indexPathForItem:0 inSection:0]
#define NavigationBar_HEIGHT 44
#define OC 255.0
#define RANDOM_NUMBER rand()%1000
#define EARTH_RADIUS 6378.138

#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion ([[UIDevice currentDevice] systemVersion])
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])

#define MINUTE_IN_SECOND 60
#define HOUR_IN_SECOND 3600
#define DAY_IN_SECOND 86400
#define MONTH_IN_SECOND 2592000
#define YEAR_IN_SECOND 94608000


static NSString *serverURLString = @"http://www.pollution.ee.unsw.edu.au";

